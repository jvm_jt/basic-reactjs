// helper for base url and endpoint string
export const baseUrl = 'http://localhost:5173/api';
export const userEndPoint = '/user';
export const postEndPoint = '/post';
