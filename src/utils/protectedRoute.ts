import { redirect } from 'react-router-dom';

export const protectedRoute = () => {
    const userToken = JSON.parse(
        localStorage.getItem('Token') || '{"username": "", "password": ""}'
    );
    if (userToken['username'] === '' && userToken['password'] === '') {
        return redirect('/login');
    } else {
        return null;
    }
};

export const isLoggedIn = () => {
    const userToken = JSON.parse(
        localStorage.getItem('Token') || '{"username": "", "password": ""}'
    );
    if (userToken['username'] !== '' && userToken['password'] !== '') {
        return redirect('/login');
    } else {
        return null;
    }
};
