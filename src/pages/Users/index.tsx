import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { IProp, IState, User, roleTypes } from './type';
import { useNavigate } from 'react-router-dom';
import { baseUrl, userEndPoint } from '../../utils/linkHelper';
import './style.css';
import {
    TextField,
    Box,
    FormControl,
    InputLabel,
    Select,
    SelectChangeEvent,
    MenuItem,
    Button,
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    Paper,
    tableCellClasses,
    styled,
    Alert,
    CircularProgress,
} from '@mui/material';

const Users = (props: IProp) => {
    const [temp_name, setTemp_name] = useState<string>('');
    const [temp_password, setTemp_password] = useState<string>('');
    const [temp_email, setTemp_email] = useState<string>('');
    const [temp_role, setTemp_role] = useState<string>('');
    // const [nameErr, setNameErr] = useState<boolean>(false);
    // const [pwErr, setPwErr] = useState<boolean>(false);
    // const [emErr, setEmErr] = useState<boolean>(false);
    // const [roleErr, setRoleErr] = useState<boolean>(false);
    const [postAlert, setPostAlert] = useState<boolean>(false);
    const [isloading, setLoading] = useState<boolean>(false);

    const [userList, setUserList] = useState<IState['userList']>([]);

    const navigate = useNavigate();

    useEffect(() => {
        handleRequestUsers();
    }, []);

    const handleRequestUsers = async () => {
        try {
            const response = await axios.get(`${baseUrl}${userEndPoint}`);
            setUserList(response.data);
        } catch (error) {
            alert('ERROR');
        }
    };

    const nameHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setTemp_name(value);
    };

    const passwordHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setTemp_password(value);
    };

    const emailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setTemp_email(value);
    };

    const roleHandler = (event: SelectChangeEvent) => {
        const { value } = event.target;
        setTemp_role(value);
    };

    const addUser = async () => {
        try {
            const newUser = {
                name: temp_name,
                password: temp_password,
                email: temp_email,
                role: temp_role,
            };
            await axios.post(`${baseUrl}${userEndPoint}`, newUser);
            handleRequestUsers();
            setTemp_name('');
            setTemp_password('');
            setTemp_email('');
            setTemp_role(roleTypes.ADMIN.toString());
            navigate('/users');
            alert('User successfully added.');
        } catch (error) {
            alert('Failed to add user.');
        }
    };

    const addButtonHandler = () => {
        setLoading(true);
        if (temp_name === '' || temp_password === '' || temp_email === '') {
            alert('Required fields are blank');
            setLoading(false);
            return;
        } else {
            // addUser();
            setPostAlert(true);
            setTimeout(() => {
                setPostAlert(false);
            }, 5000);
        }
        setLoading(false);
    };

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    return (
        <div>
            <h1>Users</h1>
            <div className="container-inputs">
                <div className="input">
                    <TextField
                        variant="outlined"
                        label="Username"
                        name="username"
                        value={temp_name}
                        onChange={nameHandler}
                    />
                </div>
                <div className="input">
                    <TextField
                        variant="outlined"
                        label="Password"
                        name="password"
                        value={temp_password}
                        onChange={passwordHandler}
                    />
                </div>
                <div className="input">
                    <TextField
                        variant="outlined"
                        label="Email"
                        name="email"
                        value={temp_email}
                        onChange={emailHandler}
                    />
                </div>
                <div className="input">
                    <Box sx={{ minWidth: 280 }}>
                        <FormControl fullWidth>
                            <InputLabel id="role-label">Role</InputLabel>
                            <Select
                                labelId="role"
                                value={temp_role}
                                label="Role"
                                onChange={roleHandler}
                            >
                                <MenuItem value={'ADMIN'}>Admin</MenuItem>
                                <MenuItem value={'LEAD'}>Lead</MenuItem>
                                <MenuItem value={'DEVELOPER'}>
                                    Developer
                                </MenuItem>
                            </Select>
                        </FormControl>
                    </Box>
                </div>
            </div>
            <Button
                variant="contained"
                color="success"
                className="btn"
                disabled={isloading}
                onClick={addButtonHandler}
                style={{ marginBottom: '8px' }}
            >
                ADD
            </Button>
            {isloading && <CircularProgress size={24} />}
            {postAlert && (
                <Alert
                    severity="success"
                    sx={{
                        fontSize: 'small',
                        padding: '4px',
                        minWidth: '200px',
                    }}
                >
                    User Successfully Added
                </Alert>
            )}
            <TableContainer component={Paper}>
                <Table
                    sx={{ minWidth: 500 }}
                    size="small"
                    className="display-table"
                >
                    <TableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ fontWeight: 'bold' }}
                                align="left"
                            >
                                NAME
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ fontWeight: 'bold' }}
                                align="left"
                            >
                                EMAIL
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ fontWeight: 'bold' }}
                                align="left"
                            >
                                ROLE
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {userList.map((items) => {
                            return (
                                <StyledTableRow
                                    key={items._id}
                                    sx={{
                                        '&:last-child td, &:last-child th': {
                                            border: 0,
                                        },
                                    }}
                                >
                                    <StyledTableCell>
                                        {items.name}
                                    </StyledTableCell>
                                    <StyledTableCell>
                                        {items.email}
                                    </StyledTableCell>
                                    <StyledTableCell>
                                        {items.role}
                                    </StyledTableCell>
                                </StyledTableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default Users;
