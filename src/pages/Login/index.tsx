import React, { useState } from 'react';
import { IProp, IState } from './type';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import './style.css';
import { baseUrl, userEndPoint } from '../../utils/linkHelper';
import { TextField, Button, Checkbox, CircularProgress } from '@mui/material';

const Login = (props: IProp) => {
    const [userList, setUserList] = useState<IState['userList']>([]);
    const [showPass, setShowPass] = useState<boolean>(false);
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [unErr, setUnErr] = useState<boolean>(false);
    const [pwErr, setPwErr] = useState<boolean>(false);
    const [_id, setID] = useState<string>('');
    const [isloading, setLoading] = useState<boolean>(false);

    const navigate = useNavigate();

    const unHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setUsername(value);
        setUnErr(false);
    };

    const pwHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setPassword(value);
        setPwErr(false);
    };

    const handleRequestUsers = async () => {
        try {
            const response = await axios.get(`${baseUrl}${userEndPoint}`);
            if (response.status === 200) {
                return response.data;
            } else {
                return [];
                alert('Error connecting to database.');
            }
        } catch (error) {
            alert(error);
        }
    };

    const loginButtonHandler = async () => {
        setLoading(true);
        const dataResponse = await handleRequestUsers();
        setUserList(dataResponse);
        if (username === '' && password === '') {
            // alert('Required fields are blank.');
            setUnErr(true);
            setPwErr(true);
            setLoading(false);
            return;
        } else if (username === '' && password !== '') {
            setUnErr(true);
            setLoading(false);
            return;
        } else if (username !== '' && password === '') {
            setPwErr(true);
            setLoading(false);
            return;
        } else {
            console.log(dataResponse);
            const authenticatedUser = dataResponse.find(
                (user: any) =>
                    user.name === username && user.password === password
            );

            if (authenticatedUser) {
                const { _id } = authenticatedUser;

                const token = {
                    username,
                    password,
                    _id,
                };

                localStorage.setItem('isLoggedIn', 'true');
                localStorage.setItem('Token', JSON.stringify(token));
                navigate('/');
            } else {
                alert('Invalid username/password.');
            }
            setLoading(false);
        }
    };

    const onKeyDownHandler = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            loginButtonHandler();
        }
    };

    return (
        <>
            <h2>Login</h2>
            <div className="basic-form-container">
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <TextField
                        error={unErr}
                        helperText={unErr ? 'Required field.' : ''}
                        id="userName"
                        label="Username"
                        variant="outlined"
                        value={username}
                        onChange={unHandler}
                        onKeyDown={onKeyDownHandler}
                    />
                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <TextField
                        error={pwErr}
                        helperText={pwErr ? 'Required field.' : ''}
                        name="password"
                        label="Password"
                        variant="outlined"
                        type={showPass ? 'text' : 'password'}
                        value={password}
                        onChange={pwHandler}
                        onKeyDown={onKeyDownHandler}
                    />
                </div>
                <Checkbox
                    checked={showPass}
                    onChange={() => setShowPass((prev) => !prev)}
                />
                <span>See Password</span>
                <Button
                    variant="contained"
                    className="btn-login"
                    onClick={loginButtonHandler}
                    disabled={isloading}
                >
                    Login
                </Button>
                {isloading ? <CircularProgress size={24} /> : 'Login'}{' '}
            </div>
        </>
    );
};

export default Login;
