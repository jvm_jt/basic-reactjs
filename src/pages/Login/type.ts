export interface IProp {}

export interface IState {
    userList: User[];
}

export interface User {
    _id: string;
    name: string;
    password: string;
    email: string;
    role: 'Admin' | 'Lead' | 'Developer' | string;
}
