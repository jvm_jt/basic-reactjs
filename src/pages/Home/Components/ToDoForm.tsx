import React from 'react';
import { toDoFormProp } from '../type';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

function ToDoForm(props: toDoFormProp) {
    const {
        inputHandler,
        messageHandler,
        btnAddHandler,
        temp_input,
        temp_message,
        inputErr,
        msgErr,
    } = props;
    return (
        <div className="basic-form-container">
            <TextField
                error={inputErr}
                helperText={inputErr ? 'Required field.' : ''}
                id="title"
                label="Title"
                variant="outlined"
                value={temp_input}
                onChange={inputHandler}
            />
            <TextField
                error={msgErr}
                helperText={msgErr ? 'Required field.' : ''}
                id="message"
                label="Message"
                multiline
                maxRows={4}
                value={temp_message}
                onChange={messageHandler}
            ></TextField>
            <Button
                variant="contained"
                color="success"
                className="btn"
                onClick={btnAddHandler}
            >
                Add
            </Button>
        </div>
    );
}

export default ToDoForm;
