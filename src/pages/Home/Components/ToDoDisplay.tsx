import React from 'react';
import { toDoDisplayProp } from '../type';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {
    TextField,
    Button,
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    Paper,
    tableCellClasses,
    styled,
} from '@mui/material';

const ToDoDisplay = (props: toDoDisplayProp) => {
    const {
        inputList,
        showTable,
        editItem,
        actionDelete,
        actionEdit,
        handleEditOnChange,
        handleUpdateButton,
        handleCancelEditButton,
    } = props;

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    return (
        <>
            {!showTable ? (
                <h5>No Todo</h5>
            ) : (
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} size="small">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell
                                    style={{ fontWeight: 'bold' }}
                                    align="left"
                                >
                                    Title
                                </StyledTableCell>
                                <StyledTableCell
                                    style={{ fontWeight: 'bold' }}
                                    align="left"
                                >
                                    Message
                                </StyledTableCell>
                                <StyledTableCell
                                    style={{ fontWeight: 'bold' }}
                                    align="center"
                                >
                                    {' '}
                                    Action
                                </StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {inputList.map((items, index) => {
                                if (items.id === editItem?.id && items.id) {
                                    return (
                                        <StyledTableRow
                                            key={items.id + index}
                                            sx={{
                                                '&:last-child td, &:last-child th':
                                                    { border: 0 },
                                            }}
                                        >
                                            <StyledTableCell align="left">
                                                <TextField
                                                    name="editInput"
                                                    variant="outlined"
                                                    label="Title"
                                                    value={editItem.input}
                                                    onChange={
                                                        handleEditOnChange
                                                    }
                                                />
                                            </StyledTableCell>
                                            <StyledTableCell align="left">
                                                <TextField
                                                    name="editMessage"
                                                    variant="outlined"
                                                    label="Message"
                                                    value={editItem.message}
                                                    onChange={
                                                        handleEditOnChange
                                                    }
                                                />
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button
                                                    variant="contained"
                                                    className="btn-update"
                                                    onClick={handleUpdateButton}
                                                >
                                                    Update
                                                </Button>
                                                <Button
                                                    variant="contained"
                                                    color="error"
                                                    className="btn-error"
                                                    onClick={
                                                        handleCancelEditButton
                                                    }
                                                >
                                                    Cancel
                                                </Button>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    );
                                } else {
                                    return (
                                        <StyledTableRow
                                            key={items.input + index}
                                        >
                                            <StyledTableCell align="left">
                                                {items.input}
                                            </StyledTableCell>
                                            <StyledTableCell align="left">
                                                {items.message}
                                            </StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button
                                                    variant="contained"
                                                    className="btn btn-primary"
                                                    startIcon={<EditIcon />}
                                                    onClick={() =>
                                                        actionEdit(items)
                                                    }
                                                >
                                                    Edit
                                                </Button>
                                                <Button
                                                    variant="contained"
                                                    color="error"
                                                    startIcon={<DeleteIcon />}
                                                    className="btn btn-error"
                                                    onClick={() =>
                                                        actionDelete(items.id)
                                                    }
                                                >
                                                    Delete
                                                </Button>
                                            </StyledTableCell>
                                        </StyledTableRow>
                                    );
                                }
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
            )}
        </>
    );
};

export default ToDoDisplay;
