export interface IProp {}

export interface IState {
    postList: ITodo[];
    editItem: ITodo;
}

export interface ITodo {
    _id: string;
    userId: string;
    title: string;
    message: string;
    status: string;
}
