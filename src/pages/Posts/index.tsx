import React, { useState, useEffect } from 'react';
import { IProp, IState, ITodo } from './type';
import './style.css';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { baseUrl, postEndPoint } from '../../utils/linkHelper';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import {
    TextField,
    Button,
    Table,
    TableBody,
    TableContainer,
    TableHead,
    TableRow,
    TableCell,
    Paper,
    tableCellClasses,
    styled,
    Alert,
} from '@mui/material';

const Post: React.FC<IProp> = () => {
    const [postList, setPostList] = useState<IState['postList']>([]);
    const [temp_userID, setTemp_userID] = useState<string>('');
    const [temp_title, setTemp_title] = useState<string>('');
    const [temp_message, setTemp_message] = useState<string>('');
    const [titleErr, setTitleErr] = useState<boolean>(false);
    const [msgErr, setMsgErr] = useState<boolean>(false);
    const [postAlert, setPostAlert] = useState<boolean>(false);

    const [editItem, setEditList] = useState<IState['editItem']>({
        _id: '',
        title: '',
        message: '',
        userId: '',
        status: '',
    });

    useEffect(() => {
        handleRequestList();
        userIdHandler();
    }, []);

    const navigate = useNavigate();

    const handleRequestList = async () => {
        try {
            const response = await axios.get(`${baseUrl}${postEndPoint}`);
            setPostList(response.data);
        } catch (error) {
            alert('ERROR');
        }
    };

    const userIdHandler = () => {
        const token = localStorage.getItem('Token');
        if (token) {
            const tokenData = JSON.parse(token);
            const userId = tokenData._id;
            setTemp_userID(userId);
        }
    };

    const titleHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setTemp_title(value);
        setTitleErr(false);
    };

    const messageHandler = (event: any) => {
        const { value } = event.target;
        setTemp_message(value);
        setMsgErr(false);
    };

    const addToDo = async () => {
        try {
            const newToDo = {
                title: temp_title,
                message: temp_message,
                userId: temp_userID,
            };
            await axios.post(`${baseUrl}${postEndPoint}`, newToDo);
            setTemp_title('');
            setTemp_message('');
            setTemp_userID('');
            handleRequestList();
            navigate('/post');
            alert('ToDo successfully added.');
        } catch (error) {
            alert('Failed to add ToDo list.');
        }
    };

    const postButtonHandler = () => {
        if (temp_title.trim() === '' && temp_message.trim() === '') {
            setTitleErr(true);
            setMsgErr(true);
            return;
        } else if (temp_title.trim() === '' && temp_message.trim() !== '') {
            setTitleErr(true);
            return;
        } else if (temp_title.trim() !== '' && temp_message.trim() === '') {
            setMsgErr(true);
            return;
        } else {
            addToDo();
            setPostAlert(true);
            setTimeout(() => {
                setPostAlert(false);
            }, 5000);
        }
    };

    const handleEditButton = (item: ITodo) => {
        setEditList({
            _id: item._id,
            title: item.title,
            message: item.message,
            userId: item.userId,
            status: item.status,
        });
    };

    const handleCancelEditButton = () => {
        setEditList({
            _id: '',
            title: '',
            message: '',
            userId: '',
            status: '',
        });
        handleRequestList();
    };

    const handleEditOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        if (name === 'editTitle') {
            setEditList({
                ...editItem,
                title: value,
            });
        } else if (name === 'editMessage') {
            setEditList({
                ...editItem,
                message: value,
            });
        }
    };

    const deleteButtonHandler = async (id: string) => {
        try {
            await axios.delete(`${baseUrl}${postEndPoint}/${id}`);
            handleRequestList();
            navigate('/post');
            alert('ToDo successfully deleted.');
        } catch (error) {
            alert('Error deleting record');
        }
    };

    const handleUpdateButton = async (id: string) => {
        try {
            const updateToDo = {
                title: editItem.title,
                message: editItem.message,
            };

            await axios.put(`${baseUrl}${postEndPoint}/${id}`, updateToDo);
            handleCancelEditButton();
            setTemp_title('');
            setTemp_message('');
            setTemp_userID('');
            navigate('/post');
            alert('Update successful.');
        } catch (error) {
            alert('Update failed.');
        }
    };

    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));

    return (
        <div>
            <h1>Post</h1>
            <div className="container-inputs">
                <div>
                    <TextField
                        error={titleErr}
                        helperText={titleErr ? 'Required field.' : ''}
                        id="title"
                        label="Title"
                        variant="outlined"
                        value={temp_title}
                        onChange={titleHandler}
                    />
                </div>
                <div>
                    <TextField
                        error={msgErr}
                        helperText={msgErr ? 'Required field.' : ''}
                        id="message"
                        label="Message"
                        multiline
                        maxRows={4}
                        value={temp_message}
                        onChange={messageHandler}
                    ></TextField>
                </div>
            </div>
            <Button
                variant="contained"
                color="success"
                className="btn"
                style={{ marginTop: '20px', marginBottom: '15px' }}
                onClick={postButtonHandler}
            >
                POST
            </Button>
            {postAlert && (
                <Alert
                    severity="success"
                    sx={{
                        fontSize: 'small',
                        padding: '4px',
                        minWidth: '200px',
                    }}
                >
                    Post Successfully Added
                </Alert>
            )}
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 500 }} size="small">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell
                                style={{ fontWeight: 'bold' }}
                                align="left"
                            >
                                Title
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ fontWeight: 'bold' }}
                                align="left"
                            >
                                Message
                            </StyledTableCell>
                            <StyledTableCell
                                style={{ fontWeight: 'bold' }}
                                align="center"
                            >
                                Action
                            </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {postList.map((items) => {
                            if (items._id === editItem?._id && items._id) {
                                return (
                                    <StyledTableRow
                                        key={items._id}
                                        sx={{
                                            '&:last-child td, &:last-child th':
                                                { border: 0 },
                                        }}
                                    >
                                        <StyledTableCell>
                                            <TextField
                                                name="editTitle"
                                                variant="outlined"
                                                label="Title"
                                                value={editItem.title}
                                                onChange={handleEditOnChange}
                                            />
                                        </StyledTableCell>
                                        <StyledTableCell>
                                            <TextField
                                                name="editMessage"
                                                variant="outlined"
                                                label="Message"
                                                value={editItem.message}
                                                onChange={handleEditOnChange}
                                            />
                                        </StyledTableCell>
                                        <StyledTableCell>
                                            <Button
                                                variant="contained"
                                                className="btn btn-primary"
                                                onClick={() =>
                                                    handleUpdateButton(
                                                        items._id
                                                    )
                                                }
                                            >
                                                Update
                                            </Button>
                                            <Button
                                                variant="contained"
                                                color="error"
                                                className="btn btn-error"
                                                onClick={() =>
                                                    handleCancelEditButton()
                                                }
                                            >
                                                Cancel
                                            </Button>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                );
                            } else {
                                if (items.status === 'ACTIVE') {
                                    return (
                                        <StyledTableRow
                                            key={items._id}
                                            sx={{
                                                '&:last-child td, &:last-child th':
                                                    { border: 0 },
                                            }}
                                        >
                                            <TableCell>{items.title}</TableCell>
                                            <TableCell>
                                                {items.message}
                                            </TableCell>
                                            <TableCell align="center">
                                                <Button
                                                    variant="contained"
                                                    startIcon={<EditIcon />}
                                                    className="btn btn-primary"
                                                    onClick={() =>
                                                        handleEditButton(items)
                                                    }
                                                >
                                                    Edit
                                                </Button>
                                                <Button
                                                    variant="contained"
                                                    color="error"
                                                    startIcon={<DeleteIcon />}
                                                    className="btn btn-error"
                                                    onClick={() =>
                                                        deleteButtonHandler(
                                                            items._id
                                                        )
                                                    }
                                                >
                                                    Delete
                                                </Button>
                                            </TableCell>
                                        </StyledTableRow>
                                    );
                                }
                            }
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
};

export default Post;
