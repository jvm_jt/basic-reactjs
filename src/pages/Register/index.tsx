import React, { useState } from 'react';
import { IProp, IState } from './type';
import { useNavigate } from 'react-router-dom';
import { accounts } from '../../utils/authenAccounts';

function Register(props: IProp) {
    const [state, setState] = useState<IState>({
        temp_un: '',
        temp_pw: '',
    });

    const navigate = useNavigate();

    const unHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, temp_un: value });
    };

    const pwHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({ ...state, temp_pw: value });
    };

    const addButtonHandler = () => {
        const { temp_un, temp_pw } = state;

        if (temp_un.trim() === '' || temp_pw.trim() === '') {
            alert('Required field/s are blank.');
            return;
        } else {
            setState((prevState) => ({
                ...prevState,
                username: temp_un,
                password: temp_pw,
            }));
            alert('Registration Successful');
            navigate('/');
        }
    };

    const backButtonHandler = () => {
        navigate('/');
    };

    return (
        <>
            <h2>Register</h2>
            <div className="register-form-container">
                <div>
                    <label>Username: </label>
                    <input
                        name="username"
                        type="text"
                        value={state.temp_un}
                        onChange={unHandler}
                        placeholder="Username"
                    />
                </div>
                <div>
                    <label>Password: </label>
                    <input
                        name="password"
                        type="text"
                        value={state.temp_pw}
                        onChange={pwHandler}
                        placeholder="Password"
                    />
                </div>
            </div>
            <div>
                <button className="btn" onClick={addButtonHandler}>
                    Add
                </button>
                <button className="btn" onClick={backButtonHandler}>
                    Back
                </button>
            </div>
        </>
    );
}

export default Register;
