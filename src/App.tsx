// import React, { useState } from 'react';
import './App.css';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { isLoggedIn, protectedRoute } from './utils/protectedRoute';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Users from './pages/Users';
import Post from './pages/Posts';
import Navbar from './components/Navbar';

const router = createBrowserRouter([
    {
        path: '/',
        element: <Home />,
        loader: protectedRoute,
    },
    {
        path: '/login',
        element: <Login />,
        loader: isLoggedIn,
    },
    {
        path: '/users',
        element: <Users />,
        loader: protectedRoute,
    },
    {
        path: '/post',
        element: <Post />,
        loader: protectedRoute,
    },
    {
        path: '/register',
        element: <Register />,
        loader: protectedRoute,
    },
    {
        path: '*',
        element: <h1>404 NOT FOUND</h1>,
    },
]);

const App = () => {
    return (
        <>
            <Navbar />
            <RouterProvider router={router} />
        </>
    );
};

export default App;
