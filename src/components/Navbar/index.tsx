import React, { useState, useEffect } from 'react';
import './style.css';
import { IProp } from './type';
import { redirect } from 'react-router-dom';
import { Link } from '@mui/material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuIcon from '@mui/icons-material/Menu';
import {
    AppBar,
    Box,
    Toolbar,
    Typography,
    Button,
    IconButton,
    Menu,
    MenuItem,
} from '@mui/material';

const Navbar = (props: IProp) => {
    const [name, setName] = useState<string>('');
    const [loggedIn, setLoggedIn] = useState<boolean>(false);
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    useEffect(() => {
        acctLabel();
    }, []);

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const logoutHandler = () => {
        localStorage.removeItem('Token');
        localStorage.removeItem('isLoggedIn');
        // redirect('/login');
        window.location.href = '/Login';
    };

    const acctLabel = () => {
        const token = localStorage.getItem('Token');
        if (token) {
            const tokenData = JSON.parse(token);
            const name = tokenData.username;
            setName(name);
        }

        const isLoggedIn = localStorage.getItem('isLoggedIn');
        if (isLoggedIn) {
            setLoggedIn(true);
        }
    };

    const homeButtonHandler = () => {
        window.location.href = '/';
    };

    const userButtonHandler = () => {
        window.location.href = '/users';
    };

    const postButtonHandler = () => {
        window.location.href = '/post';
    };

    const registerButtonHandler = () => {
        window.location.href = '/register';
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ flexGrow: 1 }}
                        style={{
                            display: 'flex',
                            justifyContent: 'left',
                            fontSize: '11px',
                            fontFamily: 'Calibri',
                        }}
                    >
                        <div className="navigation-btn">
                            <div>
                                <Button
                                    color="inherit"
                                    onClick={homeButtonHandler}
                                >
                                    Home
                                </Button>
                            </div>
                            <div>
                                <Button
                                    color="inherit"
                                    onClick={userButtonHandler}
                                >
                                    User
                                </Button>
                            </div>
                            <div>
                                <Button
                                    color="inherit"
                                    onClick={postButtonHandler}
                                >
                                    Post
                                </Button>
                            </div>
                            <div>
                                <Button
                                    color="inherit"
                                    onClick={registerButtonHandler}
                                >
                                    Register
                                </Button>
                            </div>
                        </div>
                    </Typography>
                    {loggedIn && (
                        <div className="acct-container">
                            <div
                                className="account-name"
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                }}
                            >
                                <div>
                                    <IconButton
                                        size="small"
                                        aria-label="account of current user"
                                        aria-controls="menu-appbar"
                                        aria-haspopup="true"
                                        onClick={handleMenu}
                                        color="inherit"
                                    >
                                        <AccountCircle />
                                    </IconButton>

                                    <Menu
                                        id="menu-appbar"
                                        anchorEl={anchorEl}
                                        anchorOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        keepMounted
                                        transformOrigin={{
                                            vertical: 'top',
                                            horizontal: 'right',
                                        }}
                                        open={Boolean(anchorEl)}
                                        onClose={handleClose}
                                    >
                                        <MenuItem onClick={handleClose}>
                                            Profile
                                        </MenuItem>

                                        <MenuItem onClick={logoutHandler}>
                                            Logout
                                        </MenuItem>
                                    </Menu>
                                </div>
                                <div>
                                    <label>Welcome, {name}!</label>
                                </div>
                            </div>
                        </div>
                    )}
                </Toolbar>
            </AppBar>
        </Box>
    );
};

export default Navbar;
