import React from 'react';
import { toDoDisplayProp } from '../type';

function ToDoDisplay(props: toDoDisplayProp) {
    const { inputList, showTable, actionDelete, actionEdit, actionUpdate } =
        props;

    return (
        <>
            {!showTable ? (
                <h5>No Todo</h5>
            ) : (
                <table border={1}>
                    <tbody>
                        <tr>
                            <th>Title</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                        {inputList.map((items, index) => {
                            return (
                                <tr key={items.input + index}>
                                    <td>
                                        <span>{items.input}</span>
                                    </td>
                                    <td>{items.message}</td>
                                    <td>
                                        <button
                                            key={index}
                                            className="btn btn-primary"
                                            onClick={() => actionEdit(index)}
                                        >
                                            Edit
                                        </button>
                                        <button
                                            key={index}
                                            className="btn btn-update"
                                            onClick={() => actionUpdate(index)}
                                        >
                                            Update
                                        </button>
                                        <button
                                            key={index}
                                            className="btn btn-error"
                                            onClick={() => actionDelete(index)}
                                        >
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
        </>
    );
}

export default ToDoDisplay;
