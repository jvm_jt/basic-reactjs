import React from 'react';
import { IProp, IState } from './type';
import './style.css';
import ToDoDisplay from './components/ToDoDisplay';
import ToDoForm from './components/ToDoForm';

class Home extends React.Component<IProp, IState> {
    constructor(props: IProp) {
        super(props);
        this.state = {
            temp_input: '',
            temp_message: '',
            input_list: [],
            showTable: false,
            showButton: false,
        };
    }

    inputHandler = (event: any) => {
        const { value } = event.target;
        this.setState({ temp_input: value });
    };

    messageHandler = (event: any) => {
        const { value } = event.target;
        this.setState({ temp_message: value });
    };

    toggleTable = (event: any) => {
        const { checked } = event.target;
        this.setState({ showTable: checked });
    };

    btnAddHandler = () => {
        const { temp_input, temp_message, input_list } = this.state;

        if (temp_input.trim() === '' || temp_message.trim() === '') {
            alert('Title and message fields are required.');
            return;
        } else {
            const newList = [
                ...input_list,
                {
                    input: temp_input,
                    message: temp_message,
                },
            ];
            this.setState({
                input_list: newList,
                temp_input: '',
                temp_message: '',
            });
        }
    };

    handleActionDelete = (index: number) => {
        const { input_list } = this.state;
        const newInputList = input_list.filter((item, idx) => idx !== index);
        this.setState({ input_list: newInputList });
    };

    handleActionEdit = (index: number) => {
        const { input_list } = this.state;
        this.setState({
            temp_input: input_list[index].input,
            temp_message: input_list[index].message,
        });
    };

    handleActionUpdate = (index: number) => {
        const { temp_input, temp_message, input_list } = this.state;

        if (temp_input.trim() === '' || temp_message.trim() === '') {
            alert('Title and message fields are required.');
            return;
        } else {
            const updatedList = [...input_list];
            updatedList[index].input = temp_input;
            updatedList[index].message = temp_message;

            this.setState({
                input_list: updatedList,
                temp_input: '',
                temp_message: '',
            });
        }
    };

    render() {
        return (
            <div>
                <h1>Home Page</h1>
                <div className="basic-form-container">
                    <ToDoForm
                        inputHandler={this.inputHandler}
                        messageHandler={this.messageHandler}
                        btnAddHandler={this.btnAddHandler}
                        temp_input={this.state.temp_input}
                        temp_message={this.state.temp_message}
                    />
                    <input type="checkbox" onChange={this.toggleTable} />
                    <br />
                    Show Table
                    <ToDoDisplay
                        showTable={this.state.showTable}
                        inputList={this.state.input_list}
                        actionDelete={this.handleActionDelete}
                        actionEdit={this.handleActionEdit}
                        actionUpdate={this.handleActionUpdate}
                    />
                </div>
            </div>
        );
    }
}

export default Home;
