export interface IProp {}

export interface IState {
    temp_input: string;
    temp_message: string;
    input_list: ITodo[];
    showTable: boolean;
    showButton: boolean;
}

export interface ITodo {
    input: string;
    message: string;
}

export interface toDoDisplayProp {
    inputList: ITodo[];
    showTable: boolean;
    actionDelete: (value?: any) => void;
    actionEdit: (value?: any) => void;
    actionUpdate: (value: any) => void;
}

export interface toDoFormProp {
    inputHandler: (value: any) => void;
    messageHandler: (value: any) => void;
    btnAddHandler: () => void;
    temp_input: string;
    temp_message: string;
}

export interface toEditProp {
    inputHandler: (value: any) => void;
    messageHandler: (value: any) => void;

    new_input: string;
    new_message: string;
    temp_input: string;
    temp_message: string;
}
