import React from 'react';
import { toDoDisplayProp } from '../type';

function ToDoDisplay(props: toDoDisplayProp) {
    const {
        inputList,
        showTable,
        editItem,
        actionDelete,
        actionEdit,
        handleEditOnChange,
        handleUpdateButton,
    } = props;

    return (
        <>
            {!showTable ? (
                <h5>No Todo</h5>
            ) : (
                <table border={1}>
                    <tbody>
                        <tr>
                            <th>Title</th>
                            <th>Message</th>
                            <th>Action</th>
                        </tr>
                        {inputList.map((items, index) => {
                            if (items.id === editItem?.id && items.id) {
                                return (
                                    <tr key={items.id + index}>
                                        <td>
                                            <input
                                                name="editInput"
                                                type="text"
                                                value={editItem.input}
                                                onChange={handleEditOnChange}
                                            />
                                        </td>
                                        <td>
                                            <input
                                                name="editMessage"
                                                type="text"
                                                value={editItem.message}
                                                onChange={handleEditOnChange}
                                            />
                                        </td>
                                        <td>
                                            <button
                                                name="btn"
                                                onClick={handleUpdateButton}
                                            >
                                                Update
                                            </button>
                                            <button name="btn btn-error">
                                                Cancel
                                            </button>
                                        </td>
                                    </tr>
                                );
                            } else {
                                return (
                                    <tr key={items.input + index}>
                                        <td>
                                            <span>{items.input}</span>
                                        </td>
                                        <td>{items.message}</td>
                                        <td>
                                            <button
                                                className="btn btn-primary"
                                                onClick={() =>
                                                    actionEdit(items)
                                                }
                                            >
                                                Edit
                                            </button>
                                            <button
                                                className="btn btn-error"
                                                onClick={() =>
                                                    actionDelete(items.id)
                                                }
                                            >
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                );
                            }
                        })}
                    </tbody>
                </table>
            )}
        </>
    );
}

export default ToDoDisplay;
