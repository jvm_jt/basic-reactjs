export interface IProp {}

export interface IState {
    temp_input: string;
    temp_message: string;
    input_list: ITodo[];
    showTable: boolean;
    editItem: ITodo;
}

export interface ITodo {
    id: string;
    input: string;
    message: string;
}

export interface toDoDisplayProp {
    inputList: ITodo[];
    showTable: boolean;
    actionDelete: (value?: any) => void;
    actionEdit: (value?: any) => void;
    handleEditOnChange: (event: any) => void;
    handleUpdateButton: (event: any) => void;
    editItem?: ITodo;
}

export interface toDoFormProp {
    inputHandler: (value: any) => void;
    messageHandler: (value: any) => void;
    btnAddHandler: () => void;
    temp_input: string;
    temp_message: string;
}
