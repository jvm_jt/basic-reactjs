import React from 'react';
import { IProp, IState, ITodo } from './type';
import './style.css';
import ToDoDisplay from './components/ToDoDisplay';
import ToDoForm from './components/ToDoForm';

class Home extends React.Component<IProp, IState> {
    constructor(props: IProp) {
        super(props);
        this.state = {
            temp_input: '',
            temp_message: '',
            input_list: [],
            showTable: false,
            editItem: {
                id: '',
                input: '',
                message: '',
            },
        };
    }

    inputHandler = (event: any) => {
        const { value } = event.target;
        this.setState({ temp_input: value });
    };

    messageHandler = (event: any) => {
        const { value } = event.target;
        this.setState({ temp_message: value });
    };

    btnAddHandler = () => {
        const { temp_input, temp_message, input_list } = this.state;

        if (temp_input.trim() === '' || temp_message.trim() === '') {
            alert('Title and message fields are required.');
            return;
        } else {
            const newList = [
                ...input_list,
                {
                    id: `${new Date().getTime() / 1000}`,
                    input: temp_input,
                    message: temp_message,
                },
            ];
            this.setState({
                input_list: newList,
                temp_input: '',
                temp_message: '',
            });
        }
    };

    handleCheckShowTable = (event: any) => {
        const { checked } = event.target;
        this.setState({ showTable: checked });
    };

    handleActionDelete = (id: string) => {
        const { input_list } = this.state;
        const newInputList = input_list.filter((item) => item.id !== id);
        this.setState({ input_list: newInputList });
    };

    handleEditButton = (item: ITodo) => {
        this.setState({
            editItem: { id: item.id, message: item.message, input: item.input },
        });
    };

    handleEditOnChange = (event: any) => {
        const { name, value } = event.target;
        if (name === 'editInput') {
            this.setState({
                editItem: { ...this.state.editItem, input: value },
            });
        } else if (name === 'editMessage') {
            this.setState({
                editItem: { ...this.state.editItem, message: value },
            });
        }
    };

    handleUpdateButton = () => {
        const { editItem, input_list } = this.state;

        const newList = input_list.map((items) => {
            if (items.id === editItem.id) {
                return editItem;
            }
            return items;
        });
        this.setState({
            input_list: newList,
            editItem: { id: '', input: '', message: '' },
        });
    };

    render() {
        return (
            <div>
                <h1>Home Page</h1>
                <div className="basic-form-container">
                    <ToDoForm
                        inputHandler={this.inputHandler}
                        messageHandler={this.messageHandler}
                        btnAddHandler={this.btnAddHandler}
                        temp_input={this.state.temp_input}
                        temp_message={this.state.temp_message}
                    />
                    <input
                        type="checkbox"
                        onChange={this.handleCheckShowTable}
                    />{' '}
                    <br />
                    Show Table
                    <ToDoDisplay
                        actionEdit={this.handleEditButton}
                        editItem={this.state.editItem}
                        actionDelete={this.handleActionDelete}
                        showTable={this.state.showTable}
                        inputList={this.state.input_list}
                        handleEditOnChange={this.handleEditOnChange}
                        handleUpdateButton={this.handleUpdateButton}
                    />
                </div>
            </div>
        );
    }
}

export default Home;
