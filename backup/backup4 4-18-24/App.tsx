// import { useState } from 'react'
// import reactLogo from './assets/react.svg'
// import viteLogo from '/vite.svg'
import './App.css';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { protectedRoute } from './utils/protectedRoute';

import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';

const router = createBrowserRouter([
    {
        path: '/',
        element: <Home />,
        loader: protectedRoute,
    },
    {
        path: '/login',
        element: <Login />,
    },
    {
        path: '/register',
        element: <Register />,
        loader: protectedRoute,
    },
    {
        path: '*',
        element: <h1>404 NOT FOUND</h1>,
    },
]);

function App() {
    return (
        <RouterProvider router={router} />
        // <div>
        //     <Home />
        // </div>
    );
}

export default App;
