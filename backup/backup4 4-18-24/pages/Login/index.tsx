import React, { useState } from 'react';
import { IProp } from './type';
import { useNavigate } from 'react-router-dom';
import { accounts } from '../../utils/authenAccounts';

function Login(props: IProp) {
    const [showPass, setShowPass] = useState<boolean>(false);
    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<string>('');

    const navigate = useNavigate();

    const unHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setUsername(value);
    };

    const pwHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setPassword(value);
    };

    const loginButtonHandler = () => {
        const token = {
            username,
            password,
        };

        const authenticated = accounts
            .map((item) => JSON.stringify(item))
            .includes(JSON.stringify(token));
        if (authenticated) {
            localStorage.setItem('Token', JSON.stringify(token));
            navigate('/');
        } else {
            alert('Invalid username/password.');
        }
    };

    return (
        <>
            <h2>Login</h2>
            <div className="basic-form-container">
                <input
                    name="userName"
                    type="text"
                    placeholder="Username"
                    value={username}
                    onChange={unHandler}
                />
                <input
                    name="password"
                    type={showPass ? 'text' : 'password'}
                    value={password}
                    placeholder="Password"
                    onChange={pwHandler}
                />
                <input
                    type="checkbox"
                    checked={showPass}
                    onChange={() => setShowPass((prev) => !prev)}
                />
                <span>See Password</span>
                <button className="btn-login" onClick={loginButtonHandler}>
                    {' '}
                    Login
                </button>
            </div>
        </>
    );
}

export default Login;
