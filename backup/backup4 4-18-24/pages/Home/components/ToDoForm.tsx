import React from 'react';
import { toDoFormProp } from '../type';

function ToDoForm(props: toDoFormProp) {
    const {
        inputHandler,
        messageHandler,
        btnAddHandler,
        temp_input,
        temp_message,
    } = props;
    return (
        <div>
            <input
                name="title"
                type="text"
                placeholder="Title"
                value={temp_input}
                onChange={inputHandler}
            />
            <br />
            <br />
            <textarea
                name="message"
                placeholder="Message"
                value={temp_message}
                onChange={messageHandler}
            ></textarea>
            <br />
            <br />
            <button className="btn" onClick={btnAddHandler}>
                Add
            </button>
        </div>
    );
}

export default ToDoForm;
