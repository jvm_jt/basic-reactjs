import React, { useState } from 'react';
import { IProp, IState, ITodo } from './type';
import { useNavigate } from 'react-router-dom';
import './style.css';
import ToDoDisplay from './components/ToDoDisplay';
import ToDoForm from './components/ToDoForm';

const Home: React.FC<IProp> = () => {
    const [state, setState] = useState<IState>({
        temp_input: '',
        temp_message: '',
        input_list: [],
        showTable: false,
        editItem: {
            id: '',
            input: '',
            message: '',
        },
    });

    const navigate = useNavigate();

    const logoutHandler = () => {
        localStorage.removeItem('Token');
        navigate('/login');
    };

    //  callback function to forward value of input to ToDoForm
    const inputHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        // direct update approach
        setState({
            ...state,
            temp_input: value,
        });
    };

    //  callback function to forward value of message to ToDoForm
    const messageHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { value } = event.target;
        setState({
            ...state,
            temp_message: value,
        });
    };

    //  callback function for the Add button in ToDoForm
    const btnAddHandler = () => {
        const { temp_input, temp_message, input_list } = state;

        if (temp_input.trim() === '' || temp_message.trim() === '') {
            alert('Title and message fields are required.');
            return;
        } else {
            const newList = [
                ...input_list,
                {
                    id: `${new Date().getTime() / 1000}`,
                    input: temp_input,
                    message: temp_message,
                },
            ];

            // functional update approach
            setState((prevState) => ({
                ...prevState,
                input_list: newList,
                temp_input: '',
                temp_message: '',
            }));
        }
    };

    // function to forward true/false value checked to ToDoForm
    const handleCheckShowTable = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        const { checked } = event.target;
        setState({
            ...state,
            showTable: checked,
        });
    };

    // callback function for actionDelete in onClick of Delete button in ToDoDisplay
    const handleActionDelete = (id: string) => {
        const confirmDelete = window.confirm('Proceed with deleting row?');
        if (confirmDelete) {
            const { input_list } = state;
            const newInputList = input_list.filter((item) => item.id !== id);
            setState({
                ...state,
                input_list: newInputList,
            });
        }
    };

    // (ToDoDisplay) callback function for cancel button to cancel editing of input and message textbox
    const handleCancelEditButton = () => {
        const confirmDelete = window.confirm('Do you want to cancel edit?');
        if (confirmDelete) {
            setState({
                ...state,
                editItem: {
                    id: '',
                    input: '',
                    message: '',
                },
            });
        }
    };

    // (ToDoDisplay) callback function for the edit button.
    const handleEditButton = (item: ITodo) => {
        setState({
            ...state,
            editItem: {
                id: item.id,
                message: item.message,
                input: item.input,
            },
        });
    };

    // (ToDoDisplay) callback function for onChange event in input and message text box in table
    const handleEditOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target;
        if (name === 'editInput') {
            setState({
                ...state,
                editItem: { ...state.editItem, input: value },
            });
        } else if (name === 'editMessage') {
            setState({
                ...state,
                editItem: { ...state.editItem, message: value },
            });
        }
    };

    //  (ToDoDisplay) callback function for the update button
    const handleUpdateButton = () => {
        const { editItem, input_list } = state;

        const newList = input_list.map((items) => {
            if (items.id === editItem.id) {
                return editItem;
            }
            return items;
        });
        setState({
            ...state,
            input_list: newList,
            editItem: { id: '', input: '', message: '' },
        });
    };

    // render() {
    return (
        <div>
            <button
                name="btn-logout"
                className="btn-logout"
                onClick={logoutHandler}
            >
                Log Out
            </button>
            <h1>Home Page</h1>

            <div className="basic-form-container">
                <ToDoForm
                    inputHandler={inputHandler}
                    messageHandler={messageHandler}
                    btnAddHandler={btnAddHandler}
                    temp_input={state.temp_input}
                    temp_message={state.temp_message}
                />
                <input type="checkbox" onChange={handleCheckShowTable} /> <br />
                Show Table
                <ToDoDisplay
                    actionEdit={handleEditButton}
                    editItem={state.editItem}
                    actionDelete={handleActionDelete}
                    showTable={state.showTable}
                    inputList={state.input_list}
                    handleEditOnChange={handleEditOnChange}
                    handleUpdateButton={handleUpdateButton}
                    handleCancelEditButton={handleCancelEditButton}
                />
            </div>
        </div>
    );
    // }
};

export default Home;
