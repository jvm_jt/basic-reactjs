import React from 'react';
import { IProp } from './type';

function Register(props: IProp) {
    return (
        <>
            <h2>Register</h2>
            <div className="basic-form-container">
                <input name="username" type="text" placeholder="Username" />
                <input name="password" type="text" placeholder="Password" />
            </div>
        </>
    );
}

export default Register;
